import Banner from '../components/Banner';
import { Container } from 'react-bootstrap';

export default function Error() {

   let myHero = {
      title: "404 Page not found",
      slogan: "You are trying to access a non-existing page"
   }

	return(
      
      <Container>
         <Banner data={myHero}/>
      </Container>	

	);
};
