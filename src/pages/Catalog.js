import { useEffect, useState } from 'react';
import Banner from '../components/Banner';
import GoodsCard from '../components/ProductCard';

let info = {

	title: 'OUR PRODUCTS',
	slogan: 'Pick a product you want'
}

export default function Products() {

	//working on getting the resources from the server
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://nameless-badlands-09380.herokuapp.com/product/active').then(fetchResult => fetchResult.json()).then(convertedData => {
			//console.log(convertedData);

				setProducts(convertedData.map(goods => {
					return(
						<GoodsCard key={goods._id} productInfo={goods}/>
					)
				}))

		});
	})

	return (
	  <>
		<Banner data={info}/>
		{products}
	  </>
	);
};
