import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import { Link, useParams } from 'react-router-dom';

export default function ProductView() {

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	useEffect(()=> {

			console.log(productId);

			fetch(`https://nameless-badlands-09380.herokuapp.com/product/${productId}`)
			.then(res => res.json())
			.then(data => {

				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);

			});

	}, [productId]);

	return (
		
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title> {name} </Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text> {description} </Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}  </Card.Text>
				
							<Card.Text>New harvest</Card.Text>
						
							{ (user.id) ?

								<Button variant="primary" block>Add to Cart</Button>
								:

								<Link className="btn btn-danger btn-block" to="/login">Log in to Buy</Link>
							}
						
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}

