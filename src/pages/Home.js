import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Container from 'react-bootstrap/Container';

let info = {

	title: "Welcome to Organika Grocers",
	slogan: "We provide you with fresh local products"
}

export default function Home () {
	return(
	 <>
		  <Banner data={info}/>
		  <Highlights /> 	
	 </>	
	);
};

