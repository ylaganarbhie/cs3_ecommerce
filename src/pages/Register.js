import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';


const forBanner = {
   title: "Create Your Account Here",
   slogan: "Regiter now and become an Organika!"
}


export default function Register() {

   //side effects
   const history = useHistory();
   const [ firstName, setFirstName ] = useState('');
   const [ middleName, setMiddleName ] = useState('');
   const [ lastName, setLastName ] = useState('');
   const [ email, setEmail ] = useState('');
   const [ gender, setGender ] = useState('');
   const [ password1, setPassword1 ] = useState('');
   const [ password2, setPassword2 ] = useState('');
   const [ mobileNo, setMobileNo ] = useState('');
   const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
   const [ isComplete, setIsComplete ] = useState(false);
   const [ isMatched, setIsMatched ] = useState(false);

   //swal for button
   function registerUser(event) {
      event.preventDefault(); // to avoid page reloading

   console.log(firstName);
   console.log(middleName);
   console.log(lastName);
   console.log(email);
   console.log(mobileNo);
   console.log(password1);
   console.log(gender);


      fetch(`https://nameless-badlands-09380.herokuapp.com/users/register`, {
               method: "POST",
               headers: {
                  'Content-Type': 'application/json'
               },
               body: JSON.stringify({
                  firstName: firstName,
                  middleName: middleName,
                  lastName: lastName,
                  email: email,
                  password: password1,
                  mobileNo: mobileNo,
                  gender: gender 
               })
            }).then(res=> res.json()).then(data=> {
               console.log(data)
         
                  Swal.fire({
                     title: `Hi ${firstName}, Your new account has been created`,
                     icon: 'success',
                     text: 'Welcome to Organika Grocers!'
               });
               history.push('/login');

            })
         }


   useEffect(() => {

      console.log(gender)
      if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNo !== '') && (password1 === password2))
      {
         setRegisterBtnActive(true);
         setIsComplete(true);
      }

      if ((password1 === password2) && (password1 !=='')) {
         setIsMatched(true);
      }  

      else 
      {
         setRegisterBtnActive(false);
         setIsComplete(false);
         setIsMatched(false);
      }
   });


	return(
   <>

      {/*Banner*/} 
      <Banner data={forBanner}/>

      {/* register form side effect*/}
      { isComplete ?
         <h1 className="p-3">Proceed with Register</h1>
         :
         <h1 className="p-3">Fill-up the Form Below</h1>
      }

      {/* Register Form */}
      <Form onSubmit={(e) => registerUser(e)} className="register mb-5 p-5" >

         {/*First Name*/}
         <Form.Group controlId="firstName">
            <Form.Label>
               First Name:
            </Form.Label>
            <Form.Control 

               type="text" 
               placeholder="Insert First Name Here" 
               value={firstName}
               onChange={event => setFirstName(event.target.value)}
               required
            />

         </Form.Group> 


         {/*Middle Name*/}
         <Form.Group controlId="middleName">
            <Form.Label>
               Middle Name:
            </Form.Label>
            <Form.Control 

               type="text" 
               placeholder="Insert Middle Name Here" 
               value={middleName}
               onChange={event => setMiddleName(event.target.value)}
               required
            />
         </Form.Group>


         {/*Last Name*/}
         <Form.Group controlId="lastName">
            <Form.Label>
               Last Name:
            </Form.Label>
            <Form.Control 

               type="text" 
               placeholder="Insert Last Name Here" 
               value={lastName}
               onChange={event => setLastName(event.target.value)}
               required
            />
         </Form.Group>


         {/* Email Address*/}
         <Form.Group controlId="userEmail">
            <Form.Label>
               Email Address:
            </Form.Label>
            <Form.Control 

               type="email" 
               placeholder="Insert Email Here" 
               value={email}
               onChange={event => setEmail(event.target.value)}
               required
            />
         </Form.Group>


         {/*Password*/}
         <Form.Group controlId="password1">
            <Form.Label>
               Password:
            </Form.Label>
            <Form.Control 

               type="password" 
               placeholder="Insert Password Here" 
               value={password1}
               onChange={event => setPassword1(event.target.value)}
               required
            />
         </Form.Group>

         {/* password side effect*/}
         { isMatched ?
            <p className="text-success"> *Passwords Match!* </p>
            :
            <p className="text-danger"> *Passwords Should Match!* </p>   
         }

         {/*Confirm Password*/}
         <Form.Group controlId="password2">
            <Form.Label>
               Confirm Password:
            </Form.Label>
            <Form.Control 

               type="password" 
               placeholder="Confirm Password Here" 
               value={password2}
               onChange={event => setPassword2(event.target.value)}
               required
            />
         </Form.Group>


         {/*Gender*/}
         <Form.Group controlId="gender">
            <Form.Label>
               Gender:
            </Form.Label>
            <Form.Control 
               as="select" 
               value={gender} 
               onChange={event => setGender(event.target.value)}>

               <option>Select Gender Here</option>
               <option value="Male">Male</option>               
               <option value="Female">Female</option>
            </Form.Control>   
         </Form.Group>


         {/*Mobile No.*/}
         <Form.Group>
            <Form.Label>
               Mobile Number:
            </Form.Label>
            <Form.Control 

               type="number" 
               placeholder="Insert your mobile number here" 
               value={mobileNo}
               onChange={event => setMobileNo(event.target.value)}
               required
            />
         </Form.Group>


         {/*Buttton*/}
         {isRegisterBtnActive ? 

            <Button type="submit" variant="warning" className="btn btn-block" type="submit" id="submitBtn">
               Create New Account
            </Button>
            
            :

            <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn">
               Create New Account
            </Button>    

         }

      </Form>
   </>

	);
};
