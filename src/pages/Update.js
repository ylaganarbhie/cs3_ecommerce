import { Form, Button, Container } from 'react-bootstrap';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import { useParams } from 'react-router-dom';


export default function AdminUpdate() {

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const { productId } = useParams();
	console.log({ productId })

	useEffect(()=> {

		fetch(`https://nameless-badlands-09380.herokuapp.com/product/${productId}`, {
			method: "PUT",
			headers: {
                  'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	name: name,
            	description: description,
            	price: price
            })
		}).then(res=> res.json().then(data=> {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		}))

	},[productId])

	return(
		<Form className="p-5">
			<Form.Group controlId="productName">
	            <Form.Label>
	               Product Name: {name}
	            </Form.Label>
	            <Form.Control 
	               type="text" 
	               placeholder="Product name here" 
	               required
	            />
        	 </Form.Group>

       	 	<Form.Group controlId="productDescription">
	            <Form.Label>
	               Product Description: {description}
	            </Form.Label>
	            <Form.Control 
	               type="text" 
	               placeholder="Product description here" 
	               required
	            />
        	 </Form.Group>

        	 <Form.Group controlId="productPrice">
	            <Form.Label>
	               Product Price: {price}
	            </Form.Label>
	            <Form.Control 
	               type="text" 
	               placeholder="Product price here" 
	               required
	            />
        	 </Form.Group>

        	 <Button variant="warning" className="btn btn-block" type="submit" id="submitBtn">
        	    Update
        	 </Button>



		</Form>

	)
}