import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
//import Swal from 'sweetalert2';

export default function CreateProduct(localStorage) {

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState('');
	const [ isComplete, setIsComplete ] = useState(false);

		console.log(name);
		console.log(description);
		console.log(price);
		console.log(localStorage);

	const productCreate = () => {
	
		fetch(`https://nameless-badlands-09380.herokuapp.com/product/create`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data)

		})	
	}


	
	useEffect(() => {

		if (name !=='' && description !== '' && price !=='') {
			setIsComplete(true);
		} else {
			setIsComplete(false);
		}
	})
	

	return(

	<div className= "container">	
		<Form className="form p-5">

			<Form.Group>
		       <Form.Label>Product Name: </Form.Label>
		       <Form.Control
		            type="text"
		            placeholder="Product name here"
		            value={name}
		            onChange= {event => setName(event.target.value)}
		            required
		       />
			</Form.Group>

			<Form.Group>
		       <Form.Label>Product Description: </Form.Label>
		       <Form.Control
		            type="text"
		            placeholder="Product description here"
		            value={description}
		            onChange= {event => setDescription(event.target.value)}
		            required
		       />
			</Form.Group>


			<Form.Group>			
		       <Form.Label>Product Price: </Form.Label>
		       <Form.Control
		            type="number"
		            placeholder="Product price here"
		            value={price}
		            onChange= {event => setPrice(event.target.value)}
		            required
		       />
			</Form.Group>

			{isComplete ? 

		       <Button type="submit" variant="warning" className="btn btn-block" type="submit" id="submitBtn">
		          Create New Product
		       </Button>

				:

		       <Button type="submit" variant="danger" className="btn btn-block" type="submit" id="submitBtn">
		          Create New Product
		       </Button>

			}

		 </Form>
	</div>
	)
}