import UserContext from '../UserContext';
import {useState, useEffect, useContext} from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import Banner from '../components/Banner'; 
import Swal from 'sweetalert2';


const bannerLabel = {
	title: 'Login your Account',
	slogan: 'Access your account and choose a product you want'
}

export default function Login() {

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	const authenticate = (e) => {
			e.preventDefault();

		fetch(`https://nameless-badlands-09380.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (typeof data.accessToken !== "undefined") {
				localStorage.setItem('accessToken', data.accessToken);

				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successfull",
					icon: "success",
					text: "Welcome to Organika Grocers!"
				});
			} else {
				Swal.fire({
					title: "E-mail invalid or not registered", //Authentication
					icon: "error",
					text: "Kindly check your login details and try again"
				});
			}

		})
	}

	const retrieveUserDetails = (token) => {
		fetch('https://nameless-badlands-09380.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(result => result.json()).then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})


		})
	}

	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);


	return(

		(user.id) ? 
			//"redirect the user to the products page" 
			<Redirect to="/product"/>
			:  


		<>
	        {/*Banner*/}
	   		<Banner data={bannerLabel}/>

	   		{/*Login*/}
			<Form className="mb-5 p-5" onSubmit={e => authenticate(e)}>

			    {/*User's Email*/}
				<Form.Group controlId="userEmail">
					<Form.Label> Email Address:</Form.Label>
					<Form.Control 
				   		type="email" 
				   		placeholder="Insert Email Here" 
				   		value={email}
				   		onChange={e=> setEmail(e.target.value)}
				   		required/>
				</Form.Group>
                
                {/*User's Password*/}
				<Form.Group controlId="userPassword">
					<Form.Label> Password:</Form.Label>
					<Form.Control 
				   		type="password" 
				   		placeholder="Insert Password Here"
				   		value={password}
				   		onChange={e=> setPassword(e.target.value)} 
				   		required/>
				</Form.Group>

				{/*Button*/}
				{
				isActive ? 

				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>

				:

				<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" disabled>Login</Button>

				}

			</Form>	   		
	  	</>
	);
}
