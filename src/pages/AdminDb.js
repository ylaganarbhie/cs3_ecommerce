import { useEffect, useState } from 'react';
import Admin from '../components/Admin';
import { Link } from 'react-router-dom';

export default function AdminDb() {
	
	const [products, setProducts] = useState([]);

	useEffect(() =>{
		fetch('https://nameless-badlands-09380.herokuapp.com/product/active').then(result => result.json()).then(data=> {
			setProducts(data.map(goods => {
				return(
					<Admin key={goods._id} productDetail={goods}/>
				)
			}))
		})
	})

	return(

	
			<div className="admin-top text-center ">
				<h1> Admin Dashboard </h1>
				<Link className="btn btn-primary" to="/admin/create" > Create New Product </Link>
				{products}
			</div>
		
	)
}