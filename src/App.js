import { useState, useEffect } from 'react';
import './App.css';
import Navbar from './components/AppNavBar';
import Home from './pages/Home'; 
import Catalog from './pages/Catalog';
import ProductView from './pages/ProductView';
import Admin from './pages/AdminDb';
import Update from './pages/Update';
import CreateProduct from './pages/CreateProduct';
import LogIn from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Footer from './components/Footer';
import { UserProvider } from './UserContext';
import { Route, Switch, BrowserRouter as Router} from 'react-router-dom';



function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    });
    
    //clear out the info in the local storage to logout the user
    const unsetUser = () => {
      localStorage.clear(); //omit all the items inside the local storage of the browser

    setUser({
      id: null,
      isAdmin: null
    })

  }

  useEffect(() => {

    //console.log(user);

    fetch('https://nameless-badlands-09380.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
      }      

    }).then(result=> result.json()).then(data=> {
      //console.log(data)

      if (data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })

      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  })

  return (

    <UserProvider value={{user, unsetUser, setUser}}>

      <Router>
        <Navbar />
        <div className="body">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/product" component={Catalog} />
            <Route exact path="/login" component={LogIn} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/logout" component={Logout} />
            <Route exact path="/product/:productId" component={ProductView} />
            <Route exact path="/admin" component={Admin} />
            <Route exact path="/admin/create" component={CreateProduct} />
            <Route exact path="/admin/update" component={Update} />
            <Route component={Error}/>
          </Switch>
        </div>

        <Footer />
      </Router>

    </UserProvider>
    
  );
}

export default App;
