import { Container, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function AdminRoute({productDetail}) {

	const { name, description, price} = productDetail;

	return(
		<Container>
			<Table striped bordered variant="dark" className="m-5">
				<thead>
					<tr>
					  <th>Product Name</th>
					  <th>Description</th>
					  <th>Price</th>
					  <th>Actions</th>
					</tr>  
				</thead>
				<tbody>
					<tr>
						<td>{name}</td>
						<td>{description}</td>
						<td>{price}</td>
						<td><Link className="btn btn-primary" to ="/admin/update"> Update </Link></td>
					</tr>
				</tbody>
			</Table>
		</Container>

	)
}