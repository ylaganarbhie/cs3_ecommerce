import { useContext, Fragment } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

function AppNavbar() {

   const { user } = useContext(UserContext);

	return (
		<Navbar bg="light" expand="lg">
			<Navbar.Brand>
			   
            <a className="navbar-logo">
               <img src="logo.jpg" width={100} height={70} alt="Logo"/> 
            </a>

            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav"> 

               <Nav.Link as={NavLink} to="/">Home</Nav.Link>
               <Nav.Link as={NavLink} to="/product">Products</Nav.Link>

               {(user.id) ? 
               
                     <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                  
                  :
                  <Fragment>
                     <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                     <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                  </Fragment>

               }

               {(user.isAdmin) ?

                     <Nav.Link as={NavLink} to="/admin">Admin</Nav.Link>   
                  :
                     <>
                     </>
               }

            </Navbar.Collapse>
		</Navbar>

	);
};

export default AppNavbar;
