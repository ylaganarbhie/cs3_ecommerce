import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function Footer () {
	return (
		
		<div className="footer">

		  	<Row>
		 
		    	<Col xs={12} md={6}>
		    		<p>Organika Grocers</p>
		    	</Col>
		    
		    	<Col xs={12} md={6}>
		    		<p>&copy;2021 Organika Grocers. All Rights Reserved</p>
		    	</Col>

		    </Row>


	  	</div>

	  	
	);
};

export default Footer;
