import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'; 

function Banner({data}) {

  const {title, slogan} = data;

  return(
  	<Row>
  	   <Col className="banner p-5">
  	      <h1>{title}</h1>
  	      <p>{slogan}</p>
  	   </Col>
  	</Row>

  

  );
};

export default Banner;
