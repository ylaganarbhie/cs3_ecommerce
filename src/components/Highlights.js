import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row'; 
import Col from 'react-bootstrap/Col';


function Highlights() {
  return(
    <Row className="mt-3 mb-3 p-5">
        
      <Col xs={12} md={4}>
         <Card className="Highlight p-3">
            <Card.Img variant="top" src='highlights1.jpg' alt="no image"/>
            <Card.Body>
              <Card.Title>
                Tasty Healthy Organic We Sell What's Good For You
              </Card.Title>
              <Card.Text>
                 We deliver organic fruits and vegetablesfresh from our fields to your doorsteps.                 
              </Card.Text>
            </Card.Body>
         </Card>
      </Col>
      
      <Col xs={12} md={4}>
          <Card className="Highlight p-3">
          <Card.Img variant="top" src='highlights3.jpg' alt="no image"/>
            <Card.Body>
              <Card.Title>
                Great Offers On Fruits And Vegetables
              </Card.Title>
              <Card.Text>
                We supply high quality, premium organic products.                 
              </Card.Text>
            </Card.Body>
         </Card>
      </Col>
      
      <Col xs={12} md={4}>
        <Card className="Highlight p-3">
        <Card.Img variant="top" src='highlights2.jpg' alt="no image"/>
            <Card.Body>
              <Card.Title>
                100% Organic Food
              </Card.Title>
              <Card.Text>
                 We provide a more healthier choice.                 
              </Card.Text>
            </Card.Body>
         </Card>
      </Col>
    </Row>
  );
};

export default Highlights; 
