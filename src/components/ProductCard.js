import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';


export default function ProductCard({productInfo}) {

	const { _id, name, description, price } = productInfo;

	return(
		<Card className="m-5 d-block">
			<Card.Body>
				<Card.Title> {name} </Card.Title>
				<Card.Subtitle> Product Description:</Card.Subtitle>
				<Card.Text>
				{description} 
				</Card.Text>
				<Card.Subtitle> Product Price:</Card.Subtitle>
				<Card.Text> PHP {price} </Card.Text>

				<Link className="btn btn-primary" to ={`/product/${_id}`}> See Details </Link>

			</Card.Body>
		</Card>
	);
};

